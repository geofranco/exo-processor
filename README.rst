CRP-exo-proc Project Module Repository
========================

This project processes KOR EXO xlsx files for the CRP project.
Processing is as follows: Reads in a KOR .xlsx data file, reshapes it for analysis, removes the first 25% of records and finally computes the median absolute deviation which is written to an output .xlsx file.
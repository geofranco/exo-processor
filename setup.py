# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='CRP-exo-proc',
    version='0.0.1',
    description='Project to process KOR EXO files for the CRP project',
    long_description=readme,
    author='John Franco Saraceno',
    author_email='saraceno@usgs.gov',
    url='https://github.com/geofranco/',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)


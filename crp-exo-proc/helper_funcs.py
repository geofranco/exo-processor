# -*- coding: utf-8 -*-
from __future__ import print_function

import datetime
import json
import numpy as np
import numpy.ma as ma
import os
import pandas as pd
import statsmodels.robust.scale as smc

import validictory


def process_frame(dataframe):
    frame = dataframe.copy()

#    sn_start = frame.iloc[:, 0].isin([u'EXO2 Sonde']).idxmax(axis=0,
#                                                             skipna=True)
    date_col = 'Date (MM/DD/YYYY)'
    time_col = 'Time (HH:MM:SS)'
    nrow = frame.iloc[:, 0].isin([date_col]).idxmax(axis=0, skipna=True)
# probe info
#    probe = frame.iloc[sn_start:nrow-2, 0].tolist()
#    sn = frame.iloc[sn_start:nrow-2, 1].tolist()
#    firmware = frame.iloc[sn_start:nrow-2, 2].tolist()

#    sensors = dict(zip(probe, sn))
#    sensors_fw = dict(zip(probe, firmware))
    frame = frame.iloc[nrow:, :]
    frame.columns = frame.iloc[0, :]
    # rename duplicate columns tfor sensor swaps
    cols = pd.Series(frame.columns)
    for dup in frame.columns.get_duplicates():
        cols[frame.columns.get_loc(dup)] = [dup + '.' + str(d_idx)
                                            if d_idx != 0 else dup
                                            for d_idx in range(
                                            frame.columns.get_loc(dup).sum())]
    frame.columns = cols
    frame.drop(frame.index[0], inplace=True)

    df_time = frame[time_col].copy()
    df_time = df_time.astype(str)
    frame['date'] = frame[date_col].apply(
                    lambda x: datetime.datetime.strftime(x, "%Y-%m-%d"))
    frame.index = pd.to_datetime(frame['date'] + ' ' + df_time)
    # remove columns that arent needed
    drop_cols = [date_col,
                 time_col,
                 u'Site Name',
                 u'date',
                 u'Time (Fract. Sec)',
                 u'Fault Code',
                 u'Battery V',
                 u'Cable Pwr V',
                 u'TSS mg/L',
                 u'TDS mg/L',
                 u'Press psi a',
                 u'Depth m',
                 u'Sal psu',
                 u'nLF Cond µS/cm',
                 ]

    frame = drop_columns(frame, drop_cols)
    # concat like columns with different serials from sensor swaps

    params = list(frame)
    dup_params = ['fDOM RFU',
                  u'fDOM QSU',
                  u'Chlorophyll RFU',
                  u'Chlorophyll µg/L',
                  u'BGA-PC RFU',
                  u'BGA-PC µg/L',
                  ]
    df = pd.DataFrame()
    for i in dup_params:
        if i in params:
            df[i] = collapse_cols(frame, params, i)
    # now drop any column names that have numbers in them
    for i in params:
        if hasNumbers(i):
            frame.drop(i, axis=1, inplace=True)
    return frame


def read_json_file(json_file_path):
    """This function reads in a json file and outputs the info
    as a python dictionary"""
    try:
        with open(json_file_path) as data_file:
            json_data = json.load(data_file)
    except IOError, ioex:
        print(os.strerror(ioex.errno), json_file_path, sep=' ')
    else:
        return json_data


def json_schema():
    schema = {
        "type": "object",
        "properties": {
            "mad_criteria": {
                "type": "number",
                "required": False,
                "minimum": 2,
                "maximum": 3,
            },
            "interval": {
                "type": "integer",
                "required": False
            },
            "directory": {
                "type": "string",
                "required": True
            },
            "filename": {
                "type": "string",
                "required": True
            },
            }
    }
    return schema


def validate_json(json_data_file):
    """this function validates a json file based
        on json_schema func return"""
    json_data = read_json_file(json_data_file)
    json_data = json_data[json_data.keys()[0]]
    schema = json_schema()
    return validictory.validate(json_data, schema)


def custom_mad(array_like, criteria=2.5,):
    """Function to calculate the Median Absolute Deviation of a burst.

    Parameters
    ------------
    array_like : 1-D array
    Burst data
    criteria : float
    MAD critera (2-3); higher values are less conservative

    Returns
    -------
    Mad of array : float

    """

    #TODO: Add support for all NAN arrays
    """Function to calculate the median absoilute deviation of an array
    INPUT:
    array-like
    RETURNS:
    float"""
    MAD = smc.mad(array_like)
    k = (MAD*criteria)
    M = np.nanmedian(array_like)
    high = M + k
    low = M - k
    b = ma.masked_outside(array_like, high, low)
    c = ma.compressed(b)
    return np.nanmedian(c)


def find_col_idx(params, field):
    return [i for i, s in enumerate(params) if field in s]


def collapse_cols(dataframe, params, field):
    """This function concatenates duplicate columns that occur during sensor swaps
    INPUT:
    pandas dataframe
    list of paramter column names, list like
    target field, string
    RETURNS:
    pandas dataframe"""
    frame = dataframe.copy()
    indices = find_col_idx(params, field)
    return pd.concat(frame.iloc[:, i].dropna() for i in indices).sort_index()


def hasNumbers(inputString):
    """This function identifies strings that contain numerical characters
    INPUT:
    string, string like
    RETURNS:
    bool"""
    return any(char.isdigit() for char in inputString)


def drop_columns(dataframe, cols):
    """This function drops dataframe columns contained in cols
    INPUT:
    pandas dataframe
    list of column names, list like
    RETURNS:
    pandas dataframe less columns in cols"""
    frame = dataframe.copy()
    for i in cols:
        if i in frame.columns:
            frame.drop(i, inplace=True, axis=1)
    return frame


def datetime_midpoint(dataframe):
    # midpoint of two datetime indices
    df = dataframe.copy()
    df['start'] = df.index[0]
    df['end'] = df.index[-1]
    return df.start + (df.end - df.start)/2

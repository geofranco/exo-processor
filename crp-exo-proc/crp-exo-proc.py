# -*- coding: utf-8 -*-
"""
Created on Tue Jan 12 13:58:43 2016

@author: John Franco Saraceno
@email: saraceno@usgs.gov

This software is in the public domain because it contains materials that
originally came from the United States Geological Survey,
an agency of the United States Department of Interior.
For more information, see the official USGS copyright policy at
http://www.usgs.gov/visual-id/credit_usgs.html#copyright

This software is provided "AS IS".

Script to process KOR EXO burst file
INPUT:KOR processed burst xlsx file
RETURNS: MAD xlsx file

"""
from __future__ import print_function

import argparse
import fnmatch
import os

import numpy as np
import pandas as pd

from helper_funcs import custom_mad, process_frame, read_json_file


def read_exo_file(filename):
    try:
        df = pd.read_excel(filename, header=None,)
    except (IOError, ValueError):
        print('there was an issue with:', filename, sep=' ')
    else:
        return df


def process_file(filename, json_file_name='run_params.json'):
    # TODO: check if path is file or directory (os.path.isdir() and os.path.isfile())
    print(filename)
    run_params = read_json_file(json_file_name)
    params = run_params['gov.usgs.cawsc.bgctech.crp.exo.proc']
    interval = params.get('interval', 60)
    mad_criteria = params.get('mad_criteria', 2.5)
    null_value = -9999
    df_exo = pd.read_excel(filename, header=None,)
    frame = process_frame(df_exo)

    frame.fillna(null_value, inplace=True,)
    n_samples = len(frame)
    #    ditch first quarter of data
    n_start = np.round(n_samples / 4)
    outframe = frame[n_start:].astype(float)
    grouped = outframe.groupby(pd.TimeGrouper(str(interval) + "Min"),
                               sort=False)
    # calc median absolute deviation
    exo_mad = pd.DataFrame()
    # loop through all parameters
    for i in np.arange(0, len(outframe.columns), 1):
        exo_mad[outframe.columns[i]] = grouped[outframe.columns[i]].apply(
                                        custom_mad, criteria=mad_criteria)
    exo_mad.replace(to_replace=null_value, value=np.nan, inplace=True)
    exo_mad.index.name = "Datetime (PST)"
    exo_mad['Source file'] = filename.split(os.sep)[-1]#.split('.')[0]
    return exo_mad
#    return pd.DataFrame()


def main(path):

    print('Run started, this could take a minute, please wait.....')
    # do this if the passed in path is a directory
    if os.path.isdir(path):
        files = []
        files = fnmatch.filter(os.listdir(path), '*.xlsx')
        outpath = os.path.join(path, 'output')
        if not os.path.isdir(outpath):
            os.mkdir(outpath)
        df_out = pd.concat((process_file(os.path.join(path, f))
                            for f in files))
        outfilepath = os.path.join(outpath, 'outfile.xlsx')

    # otherwise, do this if the passed in path is a single file
    else:
        filename = path
        outpath = os.sep.join([os.path.dirname(path), 'output'])
        if not os.path.isdir(outpath):
            os.mkdir(outpath)
        df_out = process_file(filename)
        outfilename = filename.replace('.xlsx',
                                       '_mad.xlsx').split(os.sep)[-1]
        outfilepath = os.path.join(outpath, outfilename)
    print('Saving output data to:', outfilepath, sep=(':'))
    df_out.to_excel(outfilepath)

    print('Run finished!!!')
# %%
if __name__ == "__main__":
    parser = argparse.ArgumentParser(
             description="KOR files")
    parser.add_argument('input', type=str)
    input_path = parser.parse_args()
    main(input_path.input)
